#
# Latest Ubuntu, patched
#
FROM ubuntu:latest
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>
RUN mv /etc/apt/sources.list /etc/apt/sources.list.DIST
RUN . /etc/lsb-release; /bin/echo -e "deb mirror://mirrors.ubuntu.com/mirrors.txt RELEASE main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt RELEASE-updates main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt RELEASE-backports main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt RELEASE-security main restricted universe multiverse\n" | sed "s/RELEASE/$DISTRIB_CODENAME/g" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get update && apt-get -y upgrade
# RUN rm /var/cache/apt/archives/*.deb
